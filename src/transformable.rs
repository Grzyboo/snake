use super::consts;

pub trait Transformable {
    fn as_pair(&self) -> (i8, i8);

    fn translate_x(&self) -> f64 {
        self.as_pair().0 as f64 * consts::TILE_SIZE
    }

    fn translate_y(&self) -> f64 {
        self.as_pair().1 as f64 * consts::TILE_SIZE
    }

    fn as_rectangle(&self) -> [f64; 4] {
        [
            self.translate_x(),
            self.translate_y(),
            consts::TILE_SIZE,
            consts::TILE_SIZE
        ]
    }
}
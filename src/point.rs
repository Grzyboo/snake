use super::transformable::Transformable;

#[derive(Clone)]
pub struct Point {
    pub x : i8,
    pub y : i8,
}

impl Point {
    pub fn collides_with(&self, point : &Point) -> bool {
        point.x == self.x && point.y == self.y
    }

    pub fn collides_with_coords(&self, x : i8, y : i8) -> bool {
        x == self.x && y == self.y
    }
}

impl Transformable for Point {
    fn as_pair(&self) -> (i8, i8) {
        (self.x, self.y)
    }
}
use super::point::Point;
use super::consts;

pub struct Snake {
    body_parts : Vec<Point>,
    movement_direction : Direction,
    last_removed_part : Option<Point>
}

impl Snake {
    pub fn new(x : i8, y : i8) -> Snake {
        let body = vec![Point {x,y}];
        Snake {
            body_parts: body,
            movement_direction: consts::DEFAULT_SNAKE_DIRECTION,
            last_removed_part: None,
        }
    }

    pub fn set_direction(&mut self, dir : Direction) {
        self.movement_direction = dir;
    }

    pub fn execute_movement(&mut self) {
        let (movement_x, movement_y) = match self.movement_direction {
            Direction::Up => (0, -1),
            Direction::Right => (1, 0),
            Direction::Down => (0, 1),
            Direction::Left => (-1, 0),
        };

        let location = self.body_parts.last().unwrap();
        let mut new_location = Point {
            x: location.x + movement_x,
            y: location.y + movement_y,
        };
        Snake::correct_out_of_window_location(&mut new_location);

        if self.body_parts.len() > 0 {
            let removed = self.body_parts.remove(0);
            self.last_removed_part = Some(removed);
        }

        self.body_parts.push(new_location);
    }

    fn correct_out_of_window_location(p : &mut Point) {
        if p.x < 0 { p.x = consts::TILES_HORIZONTAL - 1 }
        if p.y < 0 { p.y = consts::TILES_VERTICAL - 1 }

        if p.x >= consts::TILES_HORIZONTAL { p.x = 0 }
        if p.y >= consts::TILES_VERTICAL { p.y = 0 }
    }

    pub fn get_body_parts(&self) -> &Vec<Point> {
        return &self.body_parts;
    }

    pub fn add_body_part(&mut self) {
        if self.last_removed_part.is_some() {
            let p = self.last_removed_part.clone().unwrap();
            self.body_parts.insert(0, p);
            self.last_removed_part = None;
        }
    }
}

pub enum Direction {
    Up,
    Right,
    Down,
    Left
}
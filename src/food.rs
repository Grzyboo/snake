use super::point::Point;
use super::transformable::Transformable;

pub struct Food {
    location: Point
}

impl Food {
    pub fn new(x : i8, y : i8) -> Food {
        Food {
            location: Point {x, y}
        }
    }

    pub fn collides_with(&self, point : &Point) -> bool {
        self.location.collides_with(point)
    }
}

impl Transformable for Food {
    fn as_pair(&self) -> (i8, i8) {
        (self.location.x, self.location.y)
    }
}
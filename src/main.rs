extern crate piston_window;

use piston_window::*;
use snake;

fn main() {
    let mut engine = snake::game_engine::GameEngine::new();
    let mut window = create_window();

    while let Some(event) = window.next() {
        match event {
            Event::Input(input) => {
                if let Input::Button(args) = input {
                   engine.input(args);
                }
            },
            Event::Loop(loop_event) => match loop_event {
                Loop::Update(_) => {
                    engine.update();
                },
                Loop::Render(_) => {
                    window.draw_2d(&event, |context, graphics| {
                        engine.draw(context, graphics);
                    });
                },
                _ => ()
            }
            _ => ()
        }
    }
}

fn create_window() -> PistonWindow {
    use snake::consts;

    let mut event_settings = EventSettings::new();
    event_settings.max_fps = consts::WINDOW_FPS;
    event_settings.ups = consts::WINDOW_UPS;

    let window_events = Events::new(event_settings);

    let mut window: PistonWindow = WindowSettings::new(
        "Snake",
        snake::game_engine::GameEngine::get_window_size()
    ).exit_on_esc(true)
        .build()
        .unwrap();
    window.events = window_events;

    window
}
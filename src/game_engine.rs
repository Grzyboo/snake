use super::player::Snake;
use super::player::Direction;
use super::food::Food;
use super::transformable::Transformable;

use super::consts::*;

use piston_window::*;
use std::collections::HashSet;

pub struct GameEngine {
    snake : Snake,
    keys_pressed : HashSet<Key>,
    food: Vec<Food>,
}

impl GameEngine {
    pub fn new() -> GameEngine {
        let food = vec![
            Food::new(TILES_HORIZONTAL / 2, TILES_VERTICAL - 10)
        ];

        GameEngine {
            snake: Snake::new(
                TILES_HORIZONTAL / 2,
                TILES_VERTICAL - 2,
            ),
            keys_pressed: HashSet::new(),
            food,
        }
    }

    pub fn get_window_size() -> [f64; 2] {
        let size_x = (TILE_X as f64) * (TILES_HORIZONTAL as f64);
        let size_y = (TILE_Y as f64) * (TILES_VERTICAL as f64);

        [size_x, size_y]
    }
}

impl GameEngine {
    pub fn input(&mut self, args : ButtonArgs) {
        let state = args.state;
        let button = args.button;

        if let Button::Keyboard(key) = button {
            match state {
                ButtonState::Press => {
                    self.keys_pressed.insert(key);
                    self.pressed_key(key);
                },
                ButtonState::Release => {
                    self.keys_pressed.remove(&key);
                },
            }
        }
    }

    pub fn update(&mut self) {
        self.snake.execute_movement();
        self.check_collisions();
    }

    fn check_collisions(&mut self) {
        let head = self.snake.get_body_parts().last();

        if let Some(point) = head {
            let mut collided_food_index : Option<usize> = None;

            for (i, f) in self.food.iter().enumerate() {
                if f.collides_with(&point) {
                    collided_food_index = Some(i);
                    break
                }
            }

            if let Some(index) = collided_food_index {
                self.food.remove(index);
                self.snake.add_body_part();

                match self.create_new_food() {
                    Ok(f) => self.food.push(f),
                    Err(e) => println!("{}", e)
                }
            }
        }
    }

    fn create_new_food(&self) -> Result<Food, &str> {
        use rand::{thread_rng, Rng};

        let mut r = thread_rng();
        let mut placed_food = false;
        let mut tries = 0_i8;

        while !placed_food && tries < 100 {
            let x = r.gen_range(0, TILES_HORIZONTAL);
            let y = r.gen_range(0, TILES_VERTICAL);

            let body_parts = self.snake.get_body_parts();

            let mut collides_with_any_body_part = false;
            for body_part in body_parts {
                if body_part.collides_with_coords(x, y) {
                    collides_with_any_body_part = true;
                    break
                }
            }

            if !collides_with_any_body_part {
                placed_food = true;
                return Ok(Food::new(x,y))
            }

            tries += 1;
        }

        Err("For some nearly impossible reason could not create new food")
    }

    pub fn draw(&self, context : Context, graphics : &mut G2d) {
        clear([0.0, 0.0, 0.0, 1.0], graphics);

        for f in self.food.iter() {
            let rect = f.as_rectangle();
            ellipse(COLOR_FOOD, rect, context.transform, graphics);
        }

        let snake = &self.snake;
        for point in snake.get_body_parts() {
            let rect = point.as_rectangle();
            rectangle(COLOR_SNAKE, rect, context.transform, graphics);
        }
    }

    fn pressed_key(&mut self, key : Key) {
        match key {
            Key::W => self.snake.set_direction(Direction::Up),
            Key::D => self.snake.set_direction(Direction::Right),
            Key::S => self.snake.set_direction(Direction::Down),
            Key::A => self.snake.set_direction(Direction::Left),
            _ => ()
        }
    }

    pub fn get_snake(&self) -> &Snake {
        &self.snake
    }

    pub fn set_snake_direction(&mut self, dir : Direction) {
        self.snake.set_direction(dir);
    }

    pub fn translate_tile_x(value : i8) -> f64 {
        ( value * TILE_X) as f64
    }
    pub fn translate_tile_y(value : i8) -> f64 {
        ( value * TILE_Y) as f64
    }
}
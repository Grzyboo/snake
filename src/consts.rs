pub const WINDOW_FPS : u64 = 30;
pub const WINDOW_UPS : u64 = 15;

pub const COLOR_SNAKE : [f32; 4] = [0.33, 0.58, 1.0, 1.0];
pub const COLOR_FOOD : [f32; 4] = [1.0, 0.89, 0.16, 0.8];
pub const TILE_SIZE : f64 = 16.0;

pub const TILE_X: i8 = 16;
pub const TILE_Y: i8 = 16;
pub const TILES_HORIZONTAL : i8 = 40;
pub const TILES_VERTICAL : i8 = 30;

use super::player::Direction;
pub const DEFAULT_SNAKE_DIRECTION : Direction = Direction::Up;